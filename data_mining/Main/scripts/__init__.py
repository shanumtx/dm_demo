""" This program calculate mean, median, mode, standard deviation and variance of

    sample csv file data using predefined functions """

import pandas as pd
print('enter data file : ')
csv = input()
data = pd.read_csv('./data//'+csv)
mean = data.mean()
mode = data.mode(numeric_only=True).transpose()
val = mode.to_dict()
mod = pd.Series(val.get(0))
median = data.median()
std = data.std()
var = data.var()

df = pd.DataFrame({'mean': mean, 'median': median, 'mode': mod, 'std': std, 'var': var})
df.to_csv('./data//out.csv')





